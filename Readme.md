### Build status

`xfce build `[![pipeline status](https://gitlab.com/uwuleng/pardus-build/badges/xfce/pipeline.svg)](https://gitlab.com/uwuleng/pardus-build/-/pipelines?page=1&scope=all&ref=xfce) 

`lxde build `[![pipeline status](https://gitlab.com/uwuleng/pardus-build/badges/lxde/pipeline.svg)](https://gitlab.com/uwuleng/pardus-build/-/pipelines?page=1&scope=all&ref=lxde) 

`mate build `[![pipeline status](https://gitlab.com/uwuleng/pardus-build/badges/mate/pipeline.svg)](https://gitlab.com/uwuleng/pardus-build/-/pipelines?page=1&scope=all&ref=mate) 

`kde  build `[![pipeline status](https://gitlab.com/uwuleng/pardus-build/badges/kde/pipeline.svg)](https://gitlab.com/uwuleng/pardus-build/-/pipelines?page=1&scope=all&ref=kde) 


`mini build `[![pipeline status](https://gitlab.com/uwuleng/pardus-build/badges/minimal/pipeline.svg)](https://gitlab.com/uwuleng/pardus-build/-/pipelines?page=1&scope=all&ref=minimal) 

##### powered by teaiso

https://gitlab.com/tearch-linux/applications-and-tools/teaiso
